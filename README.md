# ExcelToDatabase：批量导入Excel到数据库的自动化工具

![输入图片说明](https://picx.zhimg.com/70/v2-dac160e3d3c40fbca02c8e2e40837bff_1440w.avis?source=172ae18b&biz_tag=Post)

## 简介

**ExcelToDatabase** 是一个可以批量导入excel（xls/xlsx/xlsm/xlsb/csv/txt）到数据库（mysql/oracle/sql server/postgresql/access/hive/sqlite/达梦）的自动化工具。
 **_自动_** 是其最大的特点，因为它可以根据excel自动生成表信息与数据库建立联系，最终将数据导入到数据库表。
**_批量_**是它另一个特点，因为可以做到自动化，所以你可以一次性导入成千上万张表而不是一个一个导。
**_定时_**导入导出，实时刷新，实现Excel数据与数据库表数据的无缝连接。

## 工具特色：

**自动**：工具可以根据excel自动生成表名、列名、列类型及其长度，最终创建表并导入数据，或者根据生成的表信息与数据库表自动匹配并追加或者更新数据

**批量**： 通常你只能用其他工具一个一个手动导入excel到数据库，但是现在，你可以一次性导入成千上万张表

**简单**： 只需要提供excel文件位置和目标数据库连接信息，工具就能开始工作直到excel全部被导入

**快捷**：全部导入100张每张1万行x20列x1MB的excel用时1分46秒，
导入一个100万行x50列x300MB的大型excel仅需3分24秒，
导入一个1000万行x30列x4GB的巨型csv仅需5分35秒，
导入一个10个sheet共1000万行x50列x2GB的巨型excel仅需31分25秒
（普通笔记本测试）

**智能**：你是否手动导入时经常遇到错误？不要担心！工具可以轻松避免或者自动纠正。

**定时**: 可以使用内置定时任务功能或搭配其他定时任务程序，实现定时导入

**实时**:  利用定时任务，可实现当excel数据更新时，实时同步更新至数据库

**安全**：工具无任何互联网连接程序，完全支持本地离线或局域网工作，且已通过360安全检测并上架联想应用商店，保障数据安全

## 支持环境

*   操作系统：Windows/Linux/Mac
*   Excel：xls/xlsx/xlsm/xlsb/csv/txt
*   数据库：MySQL/Oracle/SQL Server/PostgreSQL/Access/Hive/SQLite/达梦

## 官方网站&在线文档

[ExcelToDatabase-自动导入Excel文件到数据库的生产力工具](https://zhuanlan.zhihu.com/p/562690353)
